<?php

namespace app\controllers;

use app\components\Calculator;
use app\models\Result;
use DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class BackendController extends Controller {
  /**
   * {@inheritdoc}
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'allow' => TRUE,
            'roles' => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Список групп
   * @param bool $finished
   * @return string
   * @throws \Exception
   */
  public function actionIndex($finished = false) {
    $groups = Yii::$app->holyhope->getGroups($finished);

    return $this->render('index', [
      'groups' => $groups,
      'finished' => !!$finished,
    ]);
  }

  /**
   * Форма подсчёта
   * @param $id
   * @param null $calculator
   * @return string
   * @throws \ReflectionException
   * @throws \yii\web\NotFoundHttpException
   * @throws \Exception
   */
  public function actionGroup($id, $calculator = NULL) {
    $group = Yii::$app->holyhope->getGroup($id);

    if (!$group) {
      throw new NotFoundHttpException('Group is not found on Holyhope');
    }

    $students = Yii::$app->holyhope->getGroupStudents($id);

    $calculators = Calculator::calculators();

    if ($calculator) {
      if (!array_key_exists($calculator, $calculators)) {
        throw new NotFoundHttpException();
      }

      Yii::$app->view->params['back'] = ['backend/group', 'id' => $id];

      /** @var Calculator|string $calculator */
      $calculator = $calculators[$calculator];

      // Сохраняем результаты

      if (Yii::$app->request->isPost && Yii::$app->request->post('students')) {
        $result = $calculator->submit(Yii::$app->request->post(), $group);
        Yii::$app->session->addFlash('success', 'Form has been submitted, thank you!');
        return $this->redirect(['backend/result', 'id' => $result->id]);
      }

      // Начало и конец обучения

      $replace = function ($value) {
        return preg_replace('~^(\d\d)\.(\d\d)\.(\d{4})$~', '$3-$2-$1', $value);
      };

      $group['_start_date'] = min(array_map($replace, array_filter(array_column($group['ScheduleItems'] ?? [], 'BeginDate'))));
      $group['_end_date'] = max(array_map($replace, array_filter(array_column($group['ScheduleItems'] ?? [], 'EndDate'))));

      // Студенты

      $students = array_map(function ($student) {
        $student['_student'] = Yii::$app->holyhope->getStudent($student['StudentId']);
        $student['_age'] = (new DateTime(ArrayHelper::getValue($student['_student'], 'Birthday', date('c'))))->diff(new DateTime('now'))->y;
        return $student;
      }, $students);

      return $this->render('calculators/' . $calculator->getView(), [
        'group' => $group,
        'students' => $students,
        'calculator' => $calculator,
      ]);
    }

    Yii::$app->view->params['back'] = ['backend/index'];

    /** @var Result[] $results */
    $results = Result::find()->where(['group_id' => $id])->orderBy(['created_at' => SORT_DESC])->all();

    return $this->render('group', [
      'group' => $group,
      'students' => $students,
      'calculators' => $calculators,
      'results' => $results,
    ]);
  }

  /**
   * Результаты формулы и генерация сертификатов
   * @param $id
   * @throws \yii\web\NotFoundHttpException
   */
  public function actionResult($id) {
    $result = Result::findOne($id);

    if (!$result) {
      throw new NotFoundHttpException('Results not found in database');
    }

    if (!$result->group_id) {
      throw new ServerErrorHttpException('Group ID is not defined');
    }

    $group = Yii::$app->holyhope->getGroup($result->group_id);

    if (!$group) {
      throw new NotFoundHttpException('Group is not found on Holyhope');
    }

    Yii::$app->view->params['back'] = ['backend/group', 'id' => $result->group_id];

    if (Yii::$app->request->isPost && Yii::$app->request->post('students')) {
      $r = Yii::$app->request;

      $zip = $result->generateCertificates($r->post('students'), $r->post('course'), $r->post('tutor'), $r->post('assistant'), $r->post('position'), $r->post('certificate_level'));

      return Yii::$app->response->sendFile($zip);
    }

    return $this->render('result', [
      'result' => $result,
      'group' => $group,
    ]);
  }

  /**
   * @param $id
   * @return \yii\console\Response|\yii\web\Response
   * @throws \yii\web\NotFoundHttpException
   */
  public function actionDownloadResult($id) {
    $result = Result::findOne($id);

    if (!$result) {
      throw new NotFoundHttpException('Results not found in database');
    }

    if (!$result->pdf_path) {
      throw new NotFoundHttpException('Results file not found');
    }

    return Yii::$app->response->sendFile(Yii::getAlias('@files') . '/' . $result->pdf_path);
  }

  public function actionResults() {
    if (!Yii::$app->helper->isAdmin()) {
      throw new ForbiddenHttpException();
    }

    $results = Result::find()->orderBy(['created_at' => SORT_DESC])->all();

    return $this->render('results', [
      'results' => $results,
    ]);
  }
}
