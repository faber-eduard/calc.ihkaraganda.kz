<?php

namespace app\controllers;

use app\models\LoginForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller {
  /**
   * {@inheritdoc}
   */
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::class,
        'rules' => [
          [
            'actions' => ['login', 'cron'],
            'allow' => TRUE,
            'roles' => ['?'],
          ],
          [
            'allow' => TRUE,
            'roles' => ['@'],
          ],
        ],
      ],
      'verbs' => [
        'class' => VerbFilter::class,
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function actions() {
    return [
      'error' => [
        'class' => 'yii\web\ErrorAction',
      ],
    ];
  }

  /**
   * Displays homepage.
   * @return string
   */
  public function actionIndex() {
    return $this->redirect(['backend/index']);
  }

  /**
   * Login action.
   * @return Response|string
   */
  public function actionLogin() {
    if (!Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();

    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    $model->password = '';
    $this->layout = 'login';

    return $this->render('login', [
      'model' => $model,
    ]);
  }

  /**
   * Logout action.
   * @return Response
   */
  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  /**
   * Cron
   * @throws \Google_Exception
   */
  public function actionCron() {
    // Токен будет обновлен при необходимости
    Yii::$app->googleDrive->getClient();
  }

  /**
   * @throws \yii\base\InvalidConfigException
   * @throws \yii\base\InvalidRouteException
   */
  public function actionMigrate() {
    Yii::$app->response->headers->set('Content-Type', 'text/plain');
    $oldApp = Yii::$app;
    $config = require Yii::getAlias('@app') . '/config/console.php';
    new \yii\console\Application($config);
    echo Yii::$app->runAction('migrate', ['migrationPath' => '@app/migrations/', 'interactive' => FALSE]);
    Yii::$app = $oldApp;
    return;
  }

  public function actionTest() {
    $message = Yii::$app->mailer->compose();
    $message->setTextBody('This is test');
    $message->setSubject('Here is test');
    $message->setTo('reception@interschool.kz');
    $message->setFrom(Yii::$app->params['fromEmail']);
    echo (int) $message->send();
  }
}
