<?php

/**
 * @var $this \yii\web\View
 * @var $content string
 */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::$app->language; ?>">
<head>
  <meta charset="<?php echo Yii::$app->charset; ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <?php $this->registerCsrfMetaTags() ?>
  <title><?php echo Html::encode($this->title); ?></title>
  <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
  <header class="d-flex align-items-center">
    <div class="container d-flex align-items-center">
      <div class="d-flex align-items-center w-100">
        <?php if (!empty($this->params['back'])): ?>
          <div class="nav-buttons mr-5">
            <?php echo Html::a('<i class="fas fa-arrow-left"></i>', $this->params['back'], ['class' => 'back btn btn-link']); ?>

            <?php if ($this->params['back'] !== ['backend/index']): ?>
              <?php echo Html::a('<i class="fas fa-home"></i>', ['backend/index'], ['class' => 'up btn btn-link']); ?>
            <?php endif; ?>
          </div>
        <?php endif; ?>

        <div class="logo">
          <a href="<?php echo Url::to(['backend/index']) ?>"><img src="/images/logo.png" alt="" class="img-fluid" /></a>
        </div>

        <?php if (Yii::$app->helper->isAdmin()): ?>
          <div class="nav-buttons ml-auto">
            <?php echo Html::a('<i class="fas fa-list"></i>', ['backend/results'], ['class' => 'back btn btn-link']); ?>
          </div>
        <?php endif; ?>

        <?php echo Html::beginForm(['/site/logout'], 'post', ['class' => Yii::$app->helper->isAdmin() ? 'ml-1' : 'ml-auto']) . Html::submitButton('Logout <i class="fa fa-sign-out-alt"></i>', ['class' => 'btn btn-link logout']) . Html::endForm() ?>
      </div>
    </div>
  </header>

  <div class="content">
    <div class="container">
      <?php echo Alert::widget(); ?>
      <?php echo $content; ?>
    </div>
  </div>
</div>

<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>
