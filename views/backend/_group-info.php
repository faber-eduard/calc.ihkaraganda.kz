<?php

/**
 * @var $this \yii\web\View
 * @var $group array
 */

use yii\helpers\Html;

?>

<?php if ($group): ?>
  <article>
    <h3><?php echo Html::encode($group['Name'] ?? '—'); ?></h3>
    <div class="row">
      <div class="col-lg-6">
        <table class="table">
          <tbody>
          <tr>
            <th>School</th>
            <td><?php echo Html::encode($group['School'] ?? '—'); ?></td>
          </tr>
          <tr>
            <th>Discipline</th>
            <td><?php echo Html::encode($group['Discipline'] ?? '—'); ?></td>
          </tr>
          <tr>
            <th>Level</th>
            <td><?php echo Html::encode($group['Level'] ?? '—'); ?></td>
          </tr>
          <tr>
            <th>Maturity</th>
            <td><?php echo Html::encode($group['Maturity'] ?? '—'); ?></td>
          </tr>
          <tr>
            <th>Learning Type</th>
            <td><?php echo Html::encode($group['LearningType'] ?? '—'); ?></td>
          </tr>
          </tbody>
        </table>
      </div>

      <?php if (!empty($this->blocks['results'])): ?>
        <div class="col-lg-6">
          <?php echo $this->blocks['results']; ?>
        </div>
      <?php endif; ?>
    </div>
  </article>
<?php endif; ?>