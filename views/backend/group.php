<?php

/**
 * @var $this \yii\web\View
 * @var $group array
 * @var $students array
 * @var $calculators \app\components\Calculator[]
 * @var $results \app\models\Result[]
 */

use yii\helpers\Html;

$this->title = 'Group ' . ($group['Name'] ?? '???');

?>

<?php if ($results): ?>
  <?php $this->beginBlock('results'); ?>

  <h4>Previous results</h4>
  <ul>
    <?php foreach ($results as $result): ?>
      <li>
        <?php echo Html::a(Yii::$app->formatter->asDatetime($result->created_at) . ' - ' . Html::encode($result->calculator()->getLevel(TRUE) ?? $result->level), [
          'backend/result',
          'id' => $result->id,
        ]); ?>
      </li>
    <?php endforeach; ?>
  </ul>

  <?php $this->endBlock(); ?>
<?php endif; ?>

<?php //----------------------------------- ?>

<?php echo $this->render('_group-info', ['group' => $group]); ?>

<?php if ($calculators): ?>
  <article>
    <h3>Choose the formula</h3>

    <ul>
      <?php if (strtolower($group['Maturity'] ?? NULL) === 'kids'): ?>
        <li>
          <strong><?php echo Html::a('Kids', ['backend/group', 'id' => $group['Id'], 'calculator' => 'kids']); ?></strong>
        </li>
      <?php endif; ?>

      <?php foreach ($calculators as $slug => $calculator): ?>
        <li>
          <?php echo Html::a($calculator->getLevel(TRUE), ['backend/group', 'id' => $group['Id'], 'calculator' => $slug]); ?>
        </li>
      <?php endforeach; ?>
    </ul>
  </article>
<?php endif; ?>

<article>
  <?php if ($students): ?>
    <h3>Students</h3>
    <div class="table-responsive">
      <table class="table" data-toggle="table" data-search="true" data-search-align="left" data-trim-on-search="false" data-search-time-out="400" data-custom-search="customSearch" data-sort-name="0">
        <thead>
        <tr>
          <th data-sortable="true">Name</th>
          <th data-sortable="true">Transliterated Name</th>
          <th data-sortable="true">Begin Date</th>
          <th data-sortable="true">End Date</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($students as $student): ?>
          <tr>
            <td><?php echo Html::encode($student['StudentName'] ?? '—'); ?></td>
            <td><?php echo Html::encode($student['_transliterated_name'] ?? '—'); ?></td>
            <td><?php echo Html::encode($student['BeginDate'] ?? '—'); ?></td>
            <td><?php echo Html::encode($student['EndDate'] ?? '—'); ?></td>
          </tr>
        <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <?php else: ?>
    <em>Students list is empty</em>
  <?php endif; ?>
</article>
