<?php

/**
 * @var $this \yii\web\View
 * @var $group array
 * @var $students array
 * @var $calculator \app\components\calculators\YoungAdultsLevel2A2Plus
 */

use app\assets\VueAsset;
use yii\helpers\Html;

VueAsset::register($this);

$is_admin = Yii::$app->helper->isAdmin();

$this->title = 'Group ' . ($group['Name'] ?? '???');

?>

<?php echo $this->render('../_group-info', ['group' => $group]); ?>

<?php echo Html::beginForm(); ?>

<?php echo Html::hiddenInput('group_id', $group['Id']); ?>

<h1><?php echo Html::encode($calculator->getLevel(TRUE)); ?></h1>

<div class="table-responsive">
  <table class="table">
    <tr>
      <th title="ADoS: Please enter the class ref. no.">Class number</th>
      <th title="ADoS: Please enter the start date of the course">Start Date</th>
      <th title="ADoS: Please enter the end date of the course">End Date</th>
      <th title="ADoS: Please enter the coursebook">Book used</th>
      <th title="ADoS: Please enter the level">Level</th>
    </tr>
    <tr>
      <td><?php echo Html::textInput('class_number', $group['Name'], ['class' => 'form-control']); ?></td>
      <td><?php echo Html::textInput('start_date', $group['_start_date'], ['class' => 'form-control', 'type' => 'date']); ?></td>
      <td><?php echo Html::textInput('end_date', $group['_end_date'], ['class' => 'form-control', 'type' => 'date']); ?></td>
      <td><?php echo Html::textInput('book', $calculator->getBook(), ['class' => 'form-control']); ?></td>
      <td><?php echo Html::textInput('level', $calculator->getLevel(), ['class' => 'form-control']); ?></td>
    </tr>
  </table>
</div>

<div id="app" v-cloak>
  <calculator inline-template :init="<?php echo Html::encode(json_encode($students)); ?>" class="break-out">
    <div>
      <div class="table-responsive">
        <table class="table table-sm mx-auto w-auto" style="max-width: 1600px;">
          <thead>
          <tr>
            <th style="width: 1%;"></th>
            <th style="width: 300px;" title="Users: Please write the st's name as they want it to appear on the certificate.">
              Name
              <button type="button" class="btn btn-light btn-sm float-right" @click.stop="show_transliteration = !show_transliteration"><i class="fas fa-spell-check"></i></button>
            </th>
            <th v-if="show_transliteration">Transliterated Name</th>
            <th class="text-center" style="width: 60px;" title="Users: Please enter the st's age if available">Age</th>
            <th class="text-center" style="width: 80px;" title="Users: Classes attended / total no. of classes e.g. 50/60">Att ca/tc</th>
            <th class="text-center" style="width: 1%; white-space: nowrap;" title="Users: Percentage attendance">Att %</th>
            <th class="text-center" style="width: 60px;" title="Users: Homework & participation. Please give your st. a mark out of 100">HW & part/100</th>
            <th class="text-center" style="width: 60px;" title="Users: Progress tests. Please give your st. a mark out of 100">Pr./100</th>
            <th class="text-center" style="width: 60px;" title="Users: Reading. Please give your st. a mark out of 55.">R/55</th>
            <th class="text-center" style="width: 60px;" title="Users: Writing. Please give your st. a mark out of 20.">W/20</th>
            <th class="text-center" style="width: 60px;" title="Users: Listening. Please give your st. a mark out of 25.">L/25</th>
            <th class="text-center" style="width: 60px;" title="Users: Speaking. Please give your st. a mark out of 20.">S/20</th>
            <th class="text-center" style="width: 60px; white-space: nowrap;" title="Users: EOC test result/100">Total%</th>
            <th class="text-center" style="width: 60px; white-space: nowrap;" title="Users: Overall result includes EOC test + homework and participation">*Overall %</th>
            <th class="text-center" style="width: 100px;" title="Users: Pass = 65+. Certificate is number of academic hours attended (# of classes x 1.33)">Type of Certificate</th>
            <th class="text-center" style="width: 60px; white-space: nowrap;" title="Users: The next level the st will study at">New Level</th>
            <th class="text-center" style="width: 60px;" title="Indicate the number of the attempt. Remember there is only one retake of the EOC test allowed. If it's a retake, they can only get a Pass certificate.">Attempt</th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(student, id) in students" :key="'student-' + id">
            <td>
              <input type="hidden" :name="`students[${id}][_start_date]`" :value="students[id].BeginDate" autocomplete="off" />
              <input type="hidden" :name="`students[${id}][_end_date]`" :value="students[id].EndDate" autocomplete="off" />
              <input type="hidden" :name="`students[${id}][]`" :value="id + 1" autocomplete="off" />
              {{ id + 1 }}
            </td>
            <td>
              <input type="text" :name="`students[${id}][]`" :value="student.StudentName" placeholder="" class="form-control form-control-sm" required autocomplete="off" />
            </td>
            <td v-if="show_transliteration">
              {{ student._transliterated_name }}
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" v-model="student._age" placeholder="" class="form-control form-control-sm" required autocomplete="off" />
            </td>
            <td>
              <input type="text" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._att_ca_tc" autocomplete="off" />
            </td>
            <td class="align-middle text-center">
              <input type="hidden" :name="`students[${id}][]`" :value="attPercent(id) + '%'" autocomplete="off" />
              {{ attPercent(id) }}%
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._hw100" min="0" max="100" autocomplete="off" />
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._pt100" min="0" max="100" autocomplete="off" />
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._r55" min="0" max="55" autocomplete="off" />
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._w20" min="0" max="20" autocomplete="off" />
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._l25" min="0" max="25" autocomplete="off" />
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._s20" min="0" max="20" autocomplete="off" />
            </td>
            <td class="align-middle text-center">
              <!-- One empty cells -->
              <input type="hidden" :name="`students[${id}][]`" value="" autocomplete="off" />

              <input type="hidden" :name="`students[${id}][]`" :value="total(id)" autocomplete="off" />
              {{ total(id) }}
            </td>
            <td class="align-middle text-center">
              <input type="hidden" :name="`students[${id}][]`" :value="overall(id)" autocomplete="off" />
              {{ overall(id) }}
            </td>
            <td class="align-middle text-center">
              <div class="d-flex align-items-center" v-if="students[id]._modified">
                <?php if ($is_admin): ?>
                  <input type="text" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" autocomplete="off" />
                  <button type="button" class="btn btn-warning btn-sm" @click.stop="students[id]._modified = false"><i class="fas fa-lock-open"></i></button>
                <?php endif; ?>
              </div>
              <div class="d-flex align-items-center" v-else>
                <div class="flex-grow-1">
                  {{ certificate(id) }}
                </div>
                <input type="hidden" :name="`students[${id}][]`" :value="certificate(id)" autocomplete="off" />
                <?php if ($is_admin): ?>
                  <button type="button" class="btn btn-warning btn-sm" @click.stop="students[id]._modified = true"><i class="fas fa-lock"></i></button>
                <?php endif; ?>
              </div>
            </td>
            <td class="align-middle text-center">
              <input type="hidden" :name="`students[${id}][]`" :value="newLevel(id)" autocomplete="off" />
              {{ newLevel(id) }}
            </td>
            <td>
              <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="students[id]._attempt" min="0" autocomplete="off" />
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </calculator>
</div>


<?php echo Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>
<?php echo Html::endForm(); ?>

<script type="text/javascript">
  new Vue({
    el: '#app',
    components: {
      calculator: {
        props: [
          'init'
        ],
        data: function () {
          return {
            show_transliteration: false,
            students: this.init.map(function (v) {
              return Object.assign({
                _att_ca_tc: '',
                _hw100: '',
                _pt100: '',
                _r55: '',
                _w20: '',
                _l25: '',
                _s20: '',
                _attempt: 1,
                _modified: false
              }, v);
            })
          }
        },
        methods: {
          attPercent(id) {
            if (this.students[id]._att_ca_tc) {
              var formula = this.students[id]._att_ca_tc.split('/');
              if (formula.length === 2) {
                formula = formula.map(function (v) {
                  return v.toString().trim() | 0;
                });

                if (formula[1]) {
                  return Math.round(formula[0] / formula[1] * 100) | 0;
                }
              }
            }

            return 0;
          },
          total(id) {
            var student = this.students[id];
            return Math.round(((student._r55 | 0) * 0.454545 + (student._w20 | 0) * 1.25 + (student._l25 | 0) + (student._s20 | 0) * 1.25) * 10) / 10;
          },
          overall(id) {
            return Math.round((this.total(id) + (this.attPercent(id) + (this.students[id]._hw100 | 0) + (this.students[id]._pt100 | 0)) / 30) / 110 * 100);
          },
          newLevel(id) {
            return this.overall(id) >= 70 ? <?php echo json_encode($calculator->getNextLevel()); ?> : <?php echo json_encode($calculator->getLevel(NULL)); ?>;
          },
          certificate(id) {
            if (this.attPercent(id) < 65) {
              return 'Fail';
            }

            if (parseInt(this.students[id]._s20) > 11 && parseInt(this.students[id]._w20) > 11) {
              var total = this.overall(id);

              if (this.students[id]._attempt > 1 && total >= 70) {
                // return 'Pass';
              }

              if (total >= 90) {
                return 'Excellence';
              }

              if (total >= 85) {
                return 'Achievement';
              }

              if (total >= 70) {
                return 'Pass';
              }
            }

            return 'Fail';
          }
        }
      }
    }
  });
</script>
