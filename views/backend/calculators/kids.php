<?php

/**
 * @var $this \yii\web\View
 * @var $group array
 * @var $students array
 * @var $calculator \app\components\calculators\Kids
 */

use app\assets\VueAsset;
use yii\helpers\Html;

VueAsset::register($this);

$is_admin = Yii::$app->helper->isAdmin();

$this->title = 'Group ' . ($group['Name'] ?? '???');

?>

<?php echo $this->render('../_group-info', ['group' => $group]); ?>

<?php echo Html::beginForm(); ?>

<?php echo Html::hiddenInput('group_id', $group['Id']); ?>

<h1><?php echo Html::encode($calculator->getLevel(TRUE)); ?></h1>

<div class="table-responsive">
  <table class="table">
    <tr>
      <th title="ADoS: Please enter the class ref. no.">Class number</th>
      <th title="ADoS: Please enter the start date of the course">Start Date</th>
      <th title="ADoS: Please enter the end date of the course">End Date</th>
    </tr>
    <tr>
      <td><?php echo Html::textInput('class_number', $group['Name'], ['class' => 'form-control']); ?></td>
      <td><?php echo Html::textInput('start_date', $group['_start_date'], ['class' => 'form-control', 'type' => 'date']); ?></td>
      <td><?php echo Html::textInput('end_date', $group['_end_date'], ['class' => 'form-control', 'type' => 'date']); ?></td>
    </tr>
  </table>
</div>

<div id="app" v-cloak>
  <calculator inline-template :init="<?php echo Html::encode(json_encode($students)); ?>">
    <div>
      <div class="table-responsive">
        <table class="table table-sm mx-auto w-auto" style="max-width: 1600px;">
          <thead>
          <tr>
            <th style="width: 1%;"></th>
            <th title="Users: Please write the st's name as they want it to appear on the certificate.">
              Name
              <button type="button" class="btn btn-light btn-sm float-right" @click.stop="show_transliteration = !show_transliteration"><i class="fas fa-spell-check"></i></button>
            </th>
            <th v-if="show_transliteration">Transliterated Name</th>
            <th class="text-center" style="width: 100px;">Hours</th>
          </tr>
          </thead>
          <tbody>
          <tr v-for="(student, id) in students" :key="'student-' + id">
            <td>
              <input type="hidden" :name="`students[${id}][_start_date]`" :value="students[id].BeginDate" autocomplete="off" />
              <input type="hidden" :name="`students[${id}][_end_date]`" :value="students[id].EndDate" autocomplete="off" />
              <input type="hidden" :name="`students[${id}][]`" :value="id + 1" autocomplete="off" />
              {{ id + 1 }}
            </td>
            <td>
              <input type="text" :name="`students[${id}][]`" :value="student.StudentName" placeholder="" class="form-control form-control-sm" required autocomplete="off" />
            </td>
            <td v-if="show_transliteration">
              {{ student._transliterated_name }}
            </td>
            <td class="align-middle text-center">
              <div class="d-flex align-items-center" v-if="students[id]._modified">
                <?php if ($is_admin): ?>
                  <input type="number" :name="`students[${id}][]`" placeholder="" class="form-control form-control-sm" v-model="student._hours" autocomplete="off" />
                  <button type="button" class="btn btn-warning btn-sm" @click.stop="students[id]._modified = false"><i class="fas fa-lock-open"></i></button>
                <?php endif; ?>
              </div>
              <div class="d-flex align-items-center" v-else>
                <div class="flex-grow-1">
                  {{ student._hours }}
                </div>
                <input type="hidden" :name="`students[${id}][]`" :value="student._hours" autocomplete="off" />
                <?php if ($is_admin): ?>
                  <button type="button" class="btn btn-warning btn-sm" @click.stop="students[id]._modified = true"><i class="fas fa-lock"></i></button>
                <?php endif; ?>
              </div>
            </td>
          </tr>
          </tbody>
        </table>
      </div>
    </div>
  </calculator>
</div>


<?php echo Html::submitButton('Save', ['class' => 'btn btn-primary']); ?>
<?php echo Html::endForm(); ?>

<script type="text/javascript">
  new Vue({
    el: '#app',
    components: {
      calculator: {
        props: [
          'init'
        ],
        data: function () {
          return {
            show_transliteration: false,
            students: this.init.map(function (v) {
              return Object.assign({
                _hours: v.StudyUnits | 0,
                _modified: false
              }, v);
            })
          }
        }
      }
    }
  });
</script>
