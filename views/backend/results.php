<?php

/**
 * @var $this \yii\web\View
 * @var $results \app\models\Result[]
 */

use yii\helpers\Html;

$this->title = 'Results';

?>

<?php if ($results): ?>
  <h3>Results</h3>

  <div class="table-responsive">
    <table class="table" data-toggle="table" data-search="true" data-search-align="left" data-trim-on-search="false" data-search-time-out="400" data-custom-search="customSearch">
      <thead>
      <tr>
        <th data-sortable="true">Class Number</th>
        <th data-sortable="true">Formula</th>
        <th data-sortable="true">Level</th>
        <th data-sortable="true">Book</th>
        <th data-sortable="true">Start Date</th>
        <th data-sortable="true">End Date</th>
        <th data-sortable="true">Created</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($results as $result): ?>
        <tr>
          <td>
            <span hidden>
              <?php echo Html::encode(preg_replace_callback('~^(\d+)~', function ($match) {
                return str_pad($match[1], 10, 0, STR_PAD_LEFT);
              }, $result->class_number)); ?>
            </span>
            <?php echo Html::a(Html::encode($result->class_number), ['backend/result', 'id' => $result->id]); ?>
          </td>
          <td><?php echo Html::encode($result->calculator()->getLevel(TRUE) ?? '—'); ?></td>
          <td><?php echo Html::encode($result->level); ?></td>
          <td><?php echo Html::encode($result->book); ?></td>
          <td><?php echo Html::encode($result->start_date); ?></td>
          <td><?php echo Html::encode($result->end_date); ?></td>
          <td><?php echo Yii::$app->formatter->asDatetime($result->created_at, 'php:Y-m-d H:i'); ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  </div>
<?php else: ?>
  <em>Groups list is empty</em>
<?php endif; ?>




