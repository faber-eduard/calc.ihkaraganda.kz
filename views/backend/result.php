<?php

/**
 * @var $this \yii\web\View
 * @var $group array
 * @var $result \app\models\Result
 */

use yii\helpers\Html;

$this->title = 'Group ' . $group['Name'] ?? '???';

$courses = [
  'English for Kids',
  'General English',
  'English for Teens',
];

$positions = [
  'Director of Studies',
  'Assistant Director Of Studies',
  'Senior Teacher',
];

?>


<?php $this->beginBlock('results'); ?>
  <h4>Download results</h4>
  <ul>
    <li>
      <?php echo Html::a(Html::encode(basename($result->pdf_path)), ['backend/download-result', 'id' => $result->id]); ?>
    </li>
  </ul>
<?php $this->endBlock(); ?>

<?php //----------------------------------- ?>


<?php echo $this->render('_group-info', ['group' => $group]); ?>

<?php echo Html::beginForm('', 'post', ['data-dos-duration' => 5000]); ?>

<?php echo Html::hiddenInput('group_id', $group['Id']); ?>
  <h3>Certificates</h3>

  <div class="table-responsive">
    <table class="table">
      <tr>
        <th>Class number</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Book used</th>
        <th>Level</th>
      </tr>
      <tr>
        <td><?php echo Html::encode($result->class_number); ?></td>
        <td><?php echo Html::encode($result->start_date); ?></td>
        <td><?php echo Html::encode($result->end_date); ?></td>
        <td><?php echo Html::encode($result->book); ?></td>
        <td><?php echo Html::encode($result->level); ?></td>
      </tr>
    </table>
  </div>

  <!--<div class="form-group">
    <label for="course">Course: (Please make sure the correct course is specified)</label>
    <?php /*echo Html::textInput('course', !empty($group['LearningType']) ? $group['LearningType'] : $result->book, [
      'id' => 'course',
      'class' => 'form-control',
      'required' => TRUE,
    ]); */ ?>
  </div>-->

  <div class="form-group">
    <label for="course">Course:</label>
    <?php echo Html::dropDownList('course', $group['LearningType'] ?? NULL, array_combine($courses, $courses), ['id' => 'position', 'class' => 'form-control', 'required' => TRUE, 'prompt' => '']); ?>
  </div>

  <div class="form-group">
    <label for="certificate-level">Certificate Level:</label>
    <?php echo Html::textInput('certificate_level', $result->calculator()->getCertificateLevel(), [
      'id' => 'certificate-level',
      'class' => 'form-control',
      'required' => TRUE,
    ]); ?>
  </div>

  <div class="form-group">
    <label for="tutor">Course Tutor:</label>
    <?php echo Html::textInput('tutor', '', ['id' => 'tutor', 'class' => 'form-control', 'required' => TRUE]); ?>
  </div>

  <div class="form-group">
    <label for="position">Please choose the position of your line manager:</label>
    <?php echo Html::dropDownList('position', 'Assistant Director Of Studies', array_combine($positions, $positions), ['id' => 'position', 'class' => 'form-control', 'required' => TRUE]); ?>
  </div>

  <div class="form-group">
    <label for="assistant">Please type in their name:</label>
    <?php echo Html::textInput('assistant', '', ['id' => 'assistant', 'class' => 'form-control', 'required' => TRUE]); ?>
  </div>

  <h3 class="mt-5 mb-3">Students</h3>

  <div class="table-responsive">
    <table class="table table-sm mx-auto w-auto" style="max-width: 1600px;">
      <thead>
      <tr>
        <th style="width: 1%;"></th>
        <th>Name</th>
        <th>Transliterated Name</th>
        <th style="width: 190px;">Start Date</th>
        <th style="width: 190px;">End Date</th>
        <th>Result</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($result->students as $index => $student): $_result = $result->getResult($index); ?>
        <tr>
          <td><?php echo $index + 1; ?></td>
          <td><?php echo Html::encode($result->getName($index)); ?></td>
          <td>
            <?php echo Html::textInput("students[{$index}][name]", Yii::$app->helper->transliterate($result->getName($index, TRUE)), [
              'class' => 'form-control form-control-sm',
              'required' => TRUE,
            ]); ?>
          </td>
          <td>
            <?php echo Html::textInput("students[{$index}][start_date]", $student['_start_date'] ?? NULL, [
              'class' => 'form-control form-control-sm',
              'type' => 'date',
              'required' => TRUE,
            ]); ?>
          </td>
          <td>
            <?php echo Html::textInput("students[{$index}][end_date]", $student['_end_date'] ?? NULL, [
              'class' => 'form-control form-control-sm',
              'type' => 'date',
              'required' => TRUE,
            ]); ?>
          </td>
          <td>
            <label class="form-check-label w-100">
              <?php echo Html::checkbox("students[{$index}][enable]", $_result && $_result !== 'Fail'); ?>
              <?php echo Html::hiddenInput("students[{$index}][result]", $_result); ?>
              <?php echo Html::encode($_result); ?>
            </label>
          </td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>
  </div>


<?php echo Html::submitButton('Download certificates', ['class' => 'btn btn-primary']); ?>
<?php echo Html::endForm(); ?>