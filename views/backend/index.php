<?php

/**
 * @var $this \yii\web\View
 * @var $groups array
 * @var $finished bool
 */

use yii\helpers\Html;

$this->title = 'Groups';

?>

<?php if ($groups): ?>
  <h3>Choose a Group</h3>

  <?php echo yii\bootstrap4\Tabs::widget([
    'items' => [
      [
        'label' => 'Active',
        'url' => ['backend/index'],
        'active' => !$finished,
      ],
      [
        'label' => 'Finished',
        'url' => ['backend/index', 'finished' => 'yes'],
        'active' => $finished,
      ],
    ],
  ]); ?>

  <div class="table-responsive">
    <table class="table" data-toggle="table" data-search="true" data-search-align="left" data-trim-on-search="false" data-search-time-out="400" data-custom-search="customSearch">
      <thead>
      <tr>
        <th data-sortable="true">Group</th>
        <th data-sortable="true">School</th>
        <th data-sortable="true" data-searchable="false">Discipline</th>
        <th data-sortable="true">Level</th>
        <th data-sortable="true">Maturity</th>
        <th data-sortable="true">Learning Type</th>
      </tr>
      </thead>
      <tbody>
      <?php foreach ($groups as $group): ?>
        <tr>
          <td>
            <span hidden>
              <?php echo Html::encode(preg_replace_callback('~^(\d+)~', function ($match) {
                return str_pad($match[1], 10, 0, STR_PAD_LEFT);
              }, $group['Name'] ?? '—')); ?>
            </span>
            <?php echo Html::a(Html::encode($group['Name'] ?? '—'), ['backend/group', 'id' => $group['Id']]); ?>
          </td>
          <td><?php echo Html::encode($group['School'] ?? '—'); ?></td>
          <td><?php echo Html::encode($group['Discipline'] ?? '—'); ?></td>
          <td><?php echo Html::encode($group['Level'] ?? '—'); ?></td>
          <td><?php echo Html::encode($group['Maturity'] ?? '—'); ?></td>
          <td><?php echo Html::encode($group['LearningType'] ?? '—'); ?></td>
        </tr>
      <?php endforeach; ?>
      </tbody>
    </table>

  </div>
<?php else: ?>
  <em>Groups list is empty</em>
<?php endif; ?>


