<?php

/**
 * @var $this yii\web\View
 * @var $form yii\bootstrap4\ActiveForm
 * @var $model app\models\LoginForm
 */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Login';
?>
<div class="site-login">
  <h1><?php echo Html::encode($this->title) ?></h1>

  <?php $form = ActiveForm::begin([
    'id' => 'login-form',
    'layout' => 'horizontal',
    'enableClientValidation' => FALSE,
    'enableClientScript' => FALSE,
  ]); ?>

  <?php echo $form->field($model, 'username')->textInput(['autofocus' => TRUE]); ?>

  <?php echo $form->field($model, 'password')->passwordInput(); ?>

  <?php echo $form->field($model, 'rememberMe')->checkbox(); ?>

  <div class="form-group">
    <div class="col-lg-offset-1 col-lg-11">
      <?php echo Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']); ?>
    </div>
  </div>

  <?php ActiveForm::end(); ?>
</div>
