jQuery(function ($) {
  $('form').each(function () {
    var $this = $(this);
    $this.disableOnSubmit({
      duration: $this.attr('data-dos-duration') || 40000,
      buttonTemplate: '<span class="fa fa-fw fa-spinner fa-spin"></span>',
      submitInputText: 'Please Wait...'
    });
  });

});

function customSearch(data, text) {
  text = text.toLowerCase();

  return data.filter(function (row) {
    var hasText = false;
    var cell;

    for (var i in row) {
      if (row.hasOwnProperty(i) && i.substr(0, 1) !== '_') {
        cell = row[i];
        if (cell.indexOf('<') !== -1) {
          cell = $('<div></div>').html(cell).text();
        }
        if (cell.toLowerCase().indexOf(text) !== -1) {
          hasText = true;
          break;
        }
      }
    }

    return hasText;
  })
}
