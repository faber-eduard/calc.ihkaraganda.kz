<?php

$config = [
  'id' => 'basic',
  'language' => 'en',
  'name' => 'Interpress',
  'basePath' => dirname(__DIR__),
  'bootstrap' => ['log'],
  'aliases' => [
    '@bower' => '@vendor/bower-asset',
    '@npm' => '@vendor/npm-asset',
    '@runtime' => '@app/runtime',
    '@storage' => '@app/storage',
    '@files' => '@app/storage/files',
  ],
  'components' => [
    'assetManager' => [
      'appendTimestamp' => TRUE,
      'converter' => [
        'class' => \nizsheanez\assetConverter\Converter::class,
        'destinationDir' => 'assets/compiled',
        'parsers' => [
          'less' => [
            'class' => \app\components\Less::class,
            'output' => 'css',
            'options' => [
              'auto' => TRUE,
            ],
          ],
        ],
      ],
      'bundles' => [
        \yii\web\JqueryAsset::class => [
          'js' => [
            'jquery.min.js',
          ],
        ],
        \yii\bootstrap\BootstrapAsset::class => [
          'css' => [
            'css/bootstrap.min.css',
          ],
        ],
        \yii\bootstrap\BootstrapPluginAsset::class => [
          'js' => [
            'js/bootstrap.min.js',
          ],
        ],
      ],
    ],
    'request' => [
      'cookieValidationKey' => 'l9vyl6NmxuQnlJJecBHv5hAisBiz5Fy6',
    ],
    'cache' => [
      'class' => \yii\caching\FileCache::class,
    ],
    'user' => [
      'identityClass' => \app\models\User::class,
      'enableAutoLogin' => TRUE,
    ],
    'errorHandler' => [
      'errorAction' => 'site/error',
    ],
    'mailer' => [
      'class' => \yii\swiftmailer\Mailer::class,
      //'enableSwiftMailerLogging' => TRUE,
      'useFileTransport' => FALSE,
      'transport' => [
        'class' => Swift_SmtpTransport::class,
        'host' => 'smtp.send-box.ru',
        'username' => 'sendbox@ihkaraganda.kz',
        'password' => 'CbFLJDJcZPR',
        'port' => 465,
        'encryption' => 'ssl',
      ],
    ],
    'log' => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets' => [
        [
          'class' => \yii\log\FileTarget::class,
          'levels' => ['error', 'warning'],
        ],
        [
          'class' => \yii\log\FileTarget::class,
          'categories' => ['yii\swiftmailer\Logger::add'],
        ],
      ],
    ],
    'formatter' => [
      'class' => \yii\i18n\Formatter::class,
      'timeFormat' => 'php:H:i',
      'datetimeFormat' => 'php:j F, Y - H:i',
    ],
    'db' => require __DIR__ . '/db.php',
    'holyhope' => require __DIR__ . '/holyhope.php',
    'googleDrive' => [
      'class' => \app\components\GoogleDrive::class,
      'authCode' => '4/vgEfOlqueCnYJi_h6ofYsoh7bTFopBWm3-cHeCnSxZ9-r2nNc89Z3RE',
    ],
    'helper' => ['class' => \app\components\Helper::class],
    'urlManager' => [
      'enablePrettyUrl' => TRUE,
      'showScriptName' => FALSE,
      'rules' => [],
    ],
  ],
  'params' => require __DIR__ . '/params.php',
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
    // uncomment the following to add your IP if you are not connecting from localhost.
    //'allowedIPs' => ['127.0.0.1', '::1'],
  ];
}

return $config;
