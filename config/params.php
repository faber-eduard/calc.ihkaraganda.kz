<?php

return [
  'fromEmail' => 'noreply@ihkaraganda.kz',
  'calculatorEmail' => 'interpresslenina5@mail.ru',
  'calculatorEmailStep' => 'reception@interschool.kz',
  'temporaryCetificates' => TRUE,
  'users' => [
    '100' => [
      'id' => '100',
      'username' => 'admin',
      'password' => 'admin!password',
      'authKey' => 'admin',
      'accessToken' => 'admin-token',
    ],
    '101' => [
      'id' => '101',
      'username' => 'teacher',
      'password' => 'calc-teacher',
      'authKey' => 'teacher',
      'accessToken' => 'teacher-token',
    ],
  ],
];
