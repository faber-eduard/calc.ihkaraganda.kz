<?php

namespace app\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {
  public $basePath = '@webroot';
  public $baseUrl = '@web';
  public $css = [
    'css/style.less',
  ];
  public $js = [
    'js/disable-form-on-submit.min.js',
    'js/script.js',
  ];
  public $depends = [
    \yii\web\YiiAsset::class,
    \yii\bootstrap4\BootstrapAsset::class,
    BootstrapTableAsset::class,
    FontAwesomeAsset::class,
  ];
}
