<?php

namespace app\assets;

use yii\web\AssetBundle;

class BootstrapTableAsset extends AssetBundle {
  public $sourcePath = '@bower/bootstrap-table/dist';
  public $css = [
    'bootstrap-table.min.css',
  ];
  public $js = [
    'bootstrap-table.min.js',
  ];
  public $depends = [
    \yii\bootstrap4\BootstrapAsset::class,
  ];
}
