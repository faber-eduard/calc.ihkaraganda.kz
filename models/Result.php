<?php

namespace app\models;

use app\components\ActiveRecord;
use app\components\Calculator;
use FPDM;
use IndieHD\FilenameSanitizer\FilenameSanitizer;
use Yii;
use yii\behaviors\TimestampBehavior;
use ZipArchive;

/**
 * This is the model class for table "results".
 * @property int $id
 * @property int $group_id
 * @property string $class_number
 * @property string $calculator
 * @property string $start_date
 * @property string $end_date
 * @property string $book
 * @property string $level
 * @property string $students
 * @property string $pdf_path
 * @property integer $created_at
 * @property integer $updated_at
 */
class Result extends ActiveRecord {
  protected $_serialized = ['students'];

  /**
   * @inheritdoc
   */
  public function behaviors() {
    return [
      [
        'class' => TimestampBehavior::class,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function tableName() {
    return 'results';
  }

  /**
   * {@inheritdoc}
   */
  public function rules() {
    return [
      [['group_id'], 'integer'],
      [['start_date', 'end_date'], 'safe'],
      [['class_number', 'calculator', 'book', 'level', 'pdf_path'], 'string', 'max' => 255],
      [['students'], 'safe'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function attributeLabels() {
    return [
      'id' => 'ID',
      'group_id' => 'Group ID',
      'class_number' => 'Class Number',
      'calculator' => 'Calculator',
      'start_date' => 'Start Date',
      'end_date' => 'End Date',
      'book' => 'Book',
      'level' => 'Level',
      'students' => 'Students',
      'pdf_path' => 'Pdf Path',
    ];
  }

  /**
   * @return Calculator|null
   */
  public function calculator() {
    if (!$this->calculator) {
      return NULL;
    }

    try {
      $calculators = Calculator::calculators();
    }
    catch (\ReflectionException $e) {
      return NULL;
    }

    return array_key_exists($this->calculator, $calculators) ? $calculators[$this->calculator] : NULL;
  }

  /**
   * @param $student_index
   * @return mixed|null
   */
  public function getResult($student_index) {
    $student = $this->students[$student_index] ?? [];
    $result_index = $this->calculator()->getResultIndex() ?? NULL;

    if ($result_index === NULL) {
      return array_reverse(array_filter($student, function ($key) {
          return is_numeric($key);
        }, ARRAY_FILTER_USE_KEY), FALSE)[0] ?? NULL;
    }

    return $student[$result_index] ?? NULL;
  }

  /**
   * @param $student_index
   * @param bool $swap
   * @return mixed|null
   */
  public function getName($student_index, $swap = FALSE) {
    $student = $this->students[$student_index] ?? [];
    $name_index = $this->calculator()->getNameIndex() ?? NULL;

    if ($name_index === NULL) {
      $return = array_values(array_filter($student, function ($_, $key) {
          return is_numeric($key);
        }, ARRAY_FILTER_USE_BOTH))[1] ?? NULL;
    }
    else {
      $return = $student[$name_index] ?? NULL;
    }

    if ($swap && strpos($return, ' ') !== FALSE) {
      $return = implode(' ', array_reverse(array_slice(explode(' ', $return), 0, 2)));
    }

    return $return;
  }

  /**
   * @param array $students
   * @param $course
   * @param $tutor
   * @param $assistant
   * @param $position
   * @param $certificate_level
   * @return string
   * @throws \Exception
   */
  public function generateCertificates(array $students, $course, $tutor, $assistant, $position, $certificate_level) {

    $sanitizer = new FilenameSanitizer(date('Y-m-d - ', $this->created_at) . $this->calculator()->getBook(TRUE) . ' - ' . ($this->class_number ?: '') . '.zip');
    $sanitizer->stripIllegalFilesystemCharacters();
    $zipname = Yii::getAlias('@files') . '/' . $sanitizer->getFilename();

    @unlink($zipname);

    $zip = new ZipArchive;
    $res = $zip->open($zipname, ZipArchive::CREATE);

    if ($res === TRUE) {
      foreach ($students as $student) {
        if (empty($student['enable'])) {
          continue;
        }

        $sanitizer = new FilenameSanitizer($student['name'] . '.pdf');
        $sanitizer->stripIllegalFilesystemCharacters();

        $fields = [
          'Student Name' => $student['name'],
          'award in course' => $this->calculator()->getAward($student['result'], $course),
          'Level' => 'Level: ' . $certificate_level, //$this->calculator()->getCertificateLevel(),
          'From' => date('d/m/Y', strtotime($student['start_date'])),
          'To' => date('d/m/Y', strtotime($student['end_date'])),
          'Course Tutor' => $tutor,
          'ADOS' => $assistant,
          'Date' => date('d.m.Y', max(strtotime($student['end_date']), $this->created_at)),
          'Position' => $position . ':',
        ];

        $pdf = new FPDM(Yii::getAlias('@app/storage/pdf/') . $this->calculator()->getPdf());
        $pdf->Load($fields, TRUE);
        $pdf->Merge();

        $zip->addFromString($sanitizer->getFilename(), $pdf->Output('S'));
      }

      $zip->close();

      if (Yii::$app->params['temporaryCetificates']) {
        register_shutdown_function(function () use ($zipname) {
          @unlink($zipname);
        });
      }

      return $zipname;
    }
    else {
      throw new \Exception('Can not create ZIP');
    }
  }
}
