<?php

use yii\db\Migration;

/**
 * Class m191009_095916_result_date
 */
class m191009_095916_result_date extends Migration {
  /**
   * {@inheritdoc}
   */
  public function safeUp() {
    $this->addColumn('results', 'created_at', $this->integer(11)->unsigned()->null()->comment('Created'));
    $this->addColumn('results', 'updated_at', $this->integer(11)->unsigned()->null()->comment('Updated'));
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown() {
    echo "m191009_095916_result_date cannot be reverted.\n";

    return FALSE;
  }
}
