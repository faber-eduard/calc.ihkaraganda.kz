<?php

use yii\db\Migration;

/**
 * Class m191007_135459_results
 */
class m191007_135459_results extends Migration {
  /**
   * {@inheritdoc}
   */
  public function safeUp() {
    $this->createTable('results', [
      'id' => $this->primaryKey(),
      'group_id' => $this->integer()->null(),
      'class_number' => $this->string()->null(),
      'start_date' => $this->date()->null(),
      'end_date' => $this->date()->null(),
      'book' => $this->string()->null(),
      'level' => $this->string()->null(),
      'students' => $this->text()->null(),
      'pdf_path' => $this->string()->null(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown() {
    $this->dropTable('results');
  }
}
