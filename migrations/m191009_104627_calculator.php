<?php

use yii\db\Migration;

/**
 * Class m191009_104627_calculator
 */
class m191009_104627_calculator extends Migration {
  /**
   * {@inheritdoc}
   */
  public function safeUp() {
    $this->addColumn('results', 'calculator', $this->string()->null()->after('class_number'));
  }

  /**
   * {@inheritdoc}
   */
  public function safeDown() {
    echo "m191009_104627_calculator cannot be reverted.\n";

    return FALSE;
  }
}
