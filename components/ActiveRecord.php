<?php

namespace app\components;

use yii\helpers\Inflector;

abstract class ActiveRecord extends \yii\db\ActiveRecord {
  protected $_serialized = [];

  /**
   * @inheritDoc
   */
  public function __get($name) {
    $camel_name = Inflector::variablize($name);

    if (!method_exists($this, 'get' . $name) && method_exists($this, 'get' . $camel_name)) {
      $name = $camel_name;
    }

    return parent::__get($name);
  }

  /**
   * @inheritDoc
   */
  public function getAttributes($names = NULL, $except = []) {
    $values = [];
    if ($names === NULL) {
      $names = $this->attributes();
    }
    foreach ($names as $name) {
      $values[$name] = $this->$name;

      if (in_array($name, $this->_serialized, TRUE) && is_string($values[$name])) {
        $values[$name] = (array) json_decode($values[$name], TRUE);
      }
    }
    foreach ($except as $name) {
      unset($values[$name]);
    }

    return $values;
  }

  /**
   * @inheritDoc
   */
  public static function populateRecord($record, $row) {
    parent::populateRecord($record, $row);

    $_serialized = (new static)->_serialized;

    foreach ($_serialized as $name) {
      if (isset($record->$name) && is_string($record->$name)) {
        $record->$name = (array) @json_decode($record->$name, TRUE);
      }
    }

    foreach (['created_at', 'updated_at', 'submitted_at', 'commented_at'] as $name) {
      if (isset($record->$name) && is_string($record->$name)) {
        $record->$name = intval($record->$name);
      }
    }
  }

  /**
   * @inheritDoc
   */
  public function getAttribute($name) {
    $value = parent::getAttribute($name);

    if (in_array($name, $this->_serialized, TRUE) && is_string($value)) {
      $value = (array) json_decode($value, TRUE);
    }

    return $value;
  }

  /**
   * @inheritDoc
   */
  public function setAttribute($name, $value) {
    if (in_array($name, $this->_serialized, TRUE) && is_string($value)) {
      $value = (array) json_decode($value, TRUE);
    }

    parent::setAttribute($name, $value);
  }

  /**
   * @inheritDoc
   */
  public function beforeSave($insert) {
    foreach ($this->_serialized as $attribute) {
      if (is_array($this->{$attribute}) || is_object($this->{$attribute})) {
        $this->{$attribute} = json_encode($this->{$attribute}, JSON_UNESCAPED_UNICODE);
      }
    }

    return parent::beforeSave($insert);
  }

  /**
   * @inheritDoc
   */
  public function afterSave($insert, $changedAttributes) {
    foreach ($this->_serialized as $attribute) {
      if (!is_array($this->{$attribute}) || is_object($this->{$attribute})) {
        $old = $this->{$attribute};

        $this->{$attribute} = (array) @json_decode($this->{$attribute}, TRUE);

        if ($this->{$attribute} === FALSE) {
          $this->{$attribute} = $old;
        }
      }
    }

    parent::afterSave($insert, $changedAttributes);
  }

  /**
   * @param $key
   * @param $value
   * @param string $property
   */
  public function setProperty($key, $value, $property = 'data') {
    if (in_array($property, $this->_serialized, TRUE)) {
      $data = $this->getAttribute($property);

      if (!is_array($data)) {
        $data = (array) $data;
      }

      $data[$key] = $value;
      $this->{$property} = $data;
    }
    else {
      throw new \InvalidArgumentException("`{$property}` is not serialized in " . get_class($this));
    }
  }

  /**
   * @param $attribute
   */
  public function preserveData($attribute) {
    $oldValue = $this->getOldAttribute($attribute);

    if (is_string($oldValue)) {
      $oldValue = (array) json_decode($oldValue, TRUE);
    }

    $value = ArrayHelper::merge($oldValue, $this->getAttribute($attribute));
    $this->setAttribute($attribute, $value);
  }
}