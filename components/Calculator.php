<?php

namespace app\components;

use app\models\Result;
use IndieHD\FilenameSanitizer\FilenameSanitizer;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use ReflectionClass;
use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;

abstract class Calculator {
  protected static $calculators = [];

  /**
   * @return Calculator[]
   * @throws \ReflectionException
   */
  public static function calculators() {
    if (static::$calculators) {
      return static::$calculators;
    }

    foreach (glob(__DIR__ . '/calculators/*.php') as $file) {
      $class_name = basename($file, '.php');

      /** @var \app\components\Calculator $calculator */
      $calculator = (new ReflectionClass('\app\components\calculators\\' . $class_name))->newInstance();
      static::$calculators[$calculator->getSlug()] = $calculator;
    }

    uasort(static::$calculators, function (Calculator $a, Calculator $b) {
      return $a->getLevel(TRUE) <=> $b->getLevel(TRUE);
    });

    return static::$calculators;
  }

  /**
   * @param bool $long
   * @return string
   */
  abstract function getLevel($long = FALSE);

  /**
   * @return string
   */
  abstract function getCertificateLevel();

  /**
   * @param bool $age
   * @return string
   */
  abstract function getNextLevel($age = FALSE);

  /**
   * @param bool $long
   * @return string
   */
  abstract function getBook($long = FALSE);

  /**
   * @return string
   */
  abstract function getXlsx();

  /**
   * @return array
   */
  abstract function getXlsxCoordinates();

  /**
   * @return integer|null
   */
  public function getResultIndex() {
    return NULL;
  }

  /**
   * @return integer|null
   */
  public function getNameIndex() {
    return NULL;
  }

  /**
   * @return string
   * @throws \ReflectionException
   */
  public function getSlug() {
    return Inflector::camel2id((new \ReflectionClass($this))->getShortName());
  }

  /**
   * @return string
   * @throws \ReflectionException
   */
  public function getView() {
    return $this->getSlug();
  }

  /**
   * @return string
   */
  public function getPdf() {
    return 'form_adults.pdf';
  }

  /**
   * @param $result
   * @param null $course
   * @return string
   */
  public function getAward($result, $course = NULL) {
    return 'has been awarded ' . $result . ' in ' . ($course ?: $this->getBook(TRUE)) . ' Course';
  }

  /**
   * @param array $table
   * @param array $group
   * @return Result
   * @throws \Google_Exception
   * @throws \PhpOffice\PhpSpreadsheet\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
   * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
   * @throws \ReflectionException
   * @throws \yii\base\Exception
   */
  public function submit(array $table, array $group) {
    $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load(Yii::getAlias('@app/storage/xlsx/') . $this->getXlsx());
    $sheet = $spreadsheet->getActiveSheet();

    $students = $table['students'] ?? [];

    $rows = count($students);

    if ($rows) {
      $sheet_data = [];
      foreach ($students as $index => $student) {
        $sheet_data[$index] = array_filter($student, function ($_, $key) {
          return is_numeric($key);
        }, ARRAY_FILTER_USE_BOTH);
      }

      $cols = count($sheet_data[0]);
      $coordinates = $this->getXlsxCoordinates();

      $start = Coordinate::coordinateFromString($coordinates['start']);
      $start[0] = Coordinate::columnIndexFromString($start[0]);
      $range = $coordinates['start'] . ':' . Coordinate::stringFromColumnIndex($start[0] + $cols - 1) . ($start[1] + $rows - 1);

      $sheet->insertNewRowBefore($start[1] + 1, $rows - 1);
      $sheet->duplicateStyle($sheet->getStyle($coordinates['start']), $range);
      $sheet->fromArray($sheet_data, NULL, $coordinates['start']);

      for ($i = $start[1]; $i < $start[1] + $rows; $i++) {
        $sheet->getRowDimension($i)->setRowHeight(-1);
      }

      foreach (['class_number', 'start_date', 'end_date', 'book', 'level'] as $key) {
        if (!empty($coordinates[$key]) && !empty($table[$key])) {
          $sheet->setCellValue($coordinates[$key], $table[$key]);
        }
      }
    }

    $tempname = Yii::getAlias('@runtime/') . Yii::$app->security->generateRandomString() . '.xlsx';

    $writer = new Xlsx($spreadsheet);
    $writer->save($tempname);

    $sanitizer = new FilenameSanitizer(date('Y-m-d H-i - ') . $this->getBook(TRUE) . ' - ' . ($table['class_number'] ?? '') . '.pdf');
    $sanitizer->stripIllegalFilesystemCharacters();
    $pdfname = $sanitizer->getFilename();

    Yii::$app->googleDrive->xlsxToPdf($tempname, Yii::getAlias('@files') . '/' . $pdfname);

    @unlink($tempname);

    $result = new Result();
    $result->attributes = [
      'group_id' => $table['group_id'] ?? NULL,
      'class_number' => $table['class_number'] ?? NULL,
      'start_date' => $table['start_date'] ?? NULL,
      'end_date' => $table['end_date'] ?? NULL,
      'book' => $table['book'] ?? NULL,
      'level' => $table['level'] ?? NULL,
      'calculator' => $this->getSlug(),
      'students' => $students,
      'pdf_path' => $pdfname,
    ];
    $result->save();

    if ($result->firstErrors) {
      Yii::$app->session->addFlash('error', 'Could not save results: ' . array_values($result->firstErrors)[0]);
    }

    try {
      // Send email

      $body = implode(PHP_EOL, [
        $this->getLevel(TRUE),
        $this->getBook(TRUE),
        $table['class_number'] ?? '',
        '',
        !empty($result->id) ? 'Results link: ' . Url::to(['backend/result', 'id' => $result->id], TRUE) : NULL,
      ]);

      $message = Yii::$app->mailer->compose();
      $message->setTextBody($body);
      $message->setSubject($this->getLevel(TRUE) . ' ' . ($table['class_number'] ?? ''));
      $message->setTo(Yii::$app->params[($group['School'] ?? '') === 'Школа Степной 2' ? 'calculatorEmailStep' : 'calculatorEmail']);
      $message->setFrom(Yii::$app->params['fromEmail']);
      $message->attach(Yii::getAlias('@files') . '/' . $pdfname);
      $message->send();
    }
    catch (\Exception $e) {
      Yii::$app->session->addFlash('error', 'Could not send email: ' . $e->getMessage());
    }

    return $result;
  }

}