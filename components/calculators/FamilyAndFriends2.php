<?php

namespace app\components\calculators;

use app\components\Calculator;

class FamilyAndFriends2 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Family and Friends 2' : 'F&F 2';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Kids 2 (A1)';
  }

  /**
   * @inheritdoc
   */
  public function getNextLevel($age = FALSE) {
    return 'F&F 3';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Family and Friends 2';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '02_Starters_Family_and_Friends_2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
