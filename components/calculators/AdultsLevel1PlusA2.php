<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel1PlusA2 extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 1+ A2' : 'AL1+ (A2)';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 1+ A2';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Elementary II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '17_KET_Adults_1_plusA2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
