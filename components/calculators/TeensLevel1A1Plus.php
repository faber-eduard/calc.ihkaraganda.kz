<?php

namespace app\components\calculators;

use app\components\Calculator;

class TeensLevel1A1Plus extends Calculator {
  public $levelAge = 15;

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    if ($long === NULL) {
      return 'TL1';
    }
    return $long ? 'Teens level 1 (A1+)' : 'TL1 A1+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Teens 1 (A1+)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return $age ? 'YAL1+' : 'TL1+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English Plus 1 I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '08_New_Opportunities_Beginner_2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 15;
  }

}
