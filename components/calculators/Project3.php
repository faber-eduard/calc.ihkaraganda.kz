<?php

namespace app\components\calculators;

use app\components\Calculator;

class Project3 extends Calculator {
  public $levelAge = 12;

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Project 3';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Juniors 3 (A2+)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return $age ? 'TL2' : 'Project 4';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Project 3';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '06_Flyers.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
