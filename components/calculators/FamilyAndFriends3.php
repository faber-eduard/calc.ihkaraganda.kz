<?php

namespace app\components\calculators;

use app\components\Calculator;

class FamilyAndFriends3 extends Calculator {
  public $levelAge = 12;

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Family and Friends 3' : 'F&F 3';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Kids 3 (A1+)';
  }

  /**
   * @inheritdoc
   */
  public function getNextLevel($age = FALSE) {
    return $age ? 'TL1+' : 'Project 2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return $long ? 'Family and Friends 3' : 'F&F 3';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '05_Family_and_Friends_3.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
