<?php

namespace app\components\calculators;

use app\components\Calculator;

class TeensLevel2A2Plus extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Teens level 2 A2+' : 'TL2 (A2+)';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Teens 2 A2+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'TL2+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English Plus 2 I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '13_KET_Teens_Level_2_A2_plus.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
