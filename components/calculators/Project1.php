<?php

namespace app\components\calculators;

use app\components\Calculator;

class Project1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Project 1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Juniors 1 (A1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'Project 2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Project 1';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '03_Starters_Project_1.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
