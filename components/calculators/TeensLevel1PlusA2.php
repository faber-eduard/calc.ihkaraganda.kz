<?php

namespace app\components\calculators;

use app\components\Calculator;

class TeensLevel1PlusA2 extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Teens level 1+ A2' : 'TL1+ (A2)';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Teens 1+ A2';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'TL2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English Plus 1 II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '12_KET_Teens_Level_1_plusA2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
