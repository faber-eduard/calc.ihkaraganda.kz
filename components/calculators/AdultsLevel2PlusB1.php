<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel2PlusB1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 2+ B1' : 'AL2+ B1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 2+ B1';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL3';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Pre-Intermediate II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '23_PET_Adults_2_plusB1.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
