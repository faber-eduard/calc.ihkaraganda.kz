<?php

namespace app\components\calculators;

use app\components\Calculator;

class Adults3PlusB2 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 3+ B2' : 'AL3+ B2';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 3+ B2';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL4';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Intermediate II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '25_PET_Adults_3_plusB2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
