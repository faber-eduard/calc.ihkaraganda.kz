<?php

namespace app\components\calculators;

use app\components\Calculator;

class Project2 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Project 2';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Juniors 2 (A2)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'Project 3';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Project 2';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '04_Movers.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
