<?php

namespace app\components\calculators;

class LevelB1 extends Project4 {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Level B1';
  }

}
