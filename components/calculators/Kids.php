<?php

namespace app\components\calculators;

use app\components\CalculatorKids;

class Kids extends CalculatorKids {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Kids';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Level Pre-A1';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return '';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Kids';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '00_Kids.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A2',
    ];
  }

}
