<?php

namespace app\components\calculators;

use app\components\Calculator;

class YoungAdultsLevel0A1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    if ($long === NULL) {
      return 'YAL0';
    }
    return $long ? 'Young Adults level 0 (A1)' : 'YAL0 A1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults Starter (A1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'YAL1';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'New Headway Beginner';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '09_New_Headway_Beginner_Young_Adults.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A4',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 5;
  }

}
