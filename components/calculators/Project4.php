<?php

namespace app\components\calculators;

use app\components\Calculator;

class Project4 extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Project 4';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Juniors 4 (B1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'TL2+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Project 4';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '11_KET_Project_4.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
