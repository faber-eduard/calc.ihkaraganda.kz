<?php

namespace app\components\calculators;

class Juniors4 extends Project4 {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return 'Juniors 4';
  }

}
