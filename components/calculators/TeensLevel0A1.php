<?php

namespace app\components\calculators;

use app\components\Calculator;

class TeensLevel0A1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    if ($long === NULL) {
      return 'TL0';
    }
    return $long ? 'Teens level 0 (A1)' : 'TL0 A1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Teens Starter (A1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'TL1';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Wider World Starter';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '07_New_Opportunities_Beginner_1.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'level' => 'H3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
