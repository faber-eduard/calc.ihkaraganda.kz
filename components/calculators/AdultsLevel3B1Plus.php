<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel3B1Plus extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 3 B1+' : 'AL3 B1+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 3 B1+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL3+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Intermediate I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '24_PET_Adults_3_B1_plus.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
