<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel0A1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    if ($long === NULL) {
      return 'AL0';
    }
    return $long ? 'Adults level 0 (A1)' : 'AL0 A1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults Starter (A1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL1';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'New Headway Beginner';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '10_New_Headway_Beginner_Adults.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A4',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 5;
  }

}
