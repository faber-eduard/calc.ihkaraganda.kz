<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel2A2Plus extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 2 A2+' : 'AL2 A2+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 2 A2+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL2+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Pre-Intermediate I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '18_KET_Adults_2_A2_plus.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
