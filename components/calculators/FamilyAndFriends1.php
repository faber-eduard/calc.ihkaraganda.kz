<?php

namespace app\components\calculators;

use app\components\Calculator;

class FamilyAndFriends1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Family and Friends 1' : 'F&F 1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Kids 1 (Pre-A1)';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'F&F 2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return $long ? 'Family and Friends 1' : 'F&F 1';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '01_Family_and_Friends_1.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A5',
      'class_number' => 'B2',
      'start_date' => 'C2',
      'end_date' => 'D2',
      'book' => 'E2',
      'level' => 'F2',
    ];
  }

}
