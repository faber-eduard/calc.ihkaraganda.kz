<?php

namespace app\components\calculators;

use app\components\Calculator;

class YoungAdultsLevel4B2 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Young Adults level 4 B2' : 'YAL4 B2';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 4 B2';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'YAL4+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Solutions Upper-Intermediate I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '26_FCE_Young_Adults_4.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 15;
  }

}
