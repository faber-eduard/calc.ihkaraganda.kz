<?php

namespace app\components\calculators;

use app\components\Calculator;

class YoungAdultsLevel3B1Plus extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Young Adults level 3 B1+' : 'YAL3 B1+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 3 B1+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'YAL3+';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Solutions Intermediate I';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '21_PET_Young_Adults_3_B1_plus.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
