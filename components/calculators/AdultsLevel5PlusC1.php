<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel5PlusC1 extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 5+ C1' : 'AL5+ C1';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 5+ C1';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'AL6';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Advanced II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '33_CAE_Adults_5_plus.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 15;
  }

}
