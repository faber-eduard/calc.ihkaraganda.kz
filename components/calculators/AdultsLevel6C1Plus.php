<?php

namespace app\components\calculators;

use app\components\Calculator;

class AdultsLevel6C1Plus extends Calculator {
  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Adults level 6 C1+' : 'AL6 C1+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 6 C1+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return '-';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English File Advanced III';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '35_CAE_Adults_6.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 15;
  }

}
