<?php

namespace app\components\calculators;

use app\components\Calculator;

class TeensLevel2PlusB1 extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Teens level 2+ B1' : 'TL2+ (B1)';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Teens 2+ B1';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'TL3';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'English Plus 2 II';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '14_KET_Teens_Level_2_plusB1.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 14;
  }

}
