<?php

namespace app\components\calculators;

use app\components\Calculator;

class YoungAdults1A1Plus extends Calculator {

  /**
   * @inheritdoc
   */
  function getLevel($long = FALSE) {
    return $long ? 'Young Adults level 1 A1+' : 'YAL1 A1+';
  }

  /**
   * @inheritdoc
   */
  function getCertificateLevel($age = FALSE) {
    return 'Adults 1 A1+';
  }

  /**
   * @inheritdoc
   */
  function getNextLevel($age = FALSE) {
    return 'YAL1+ A2';
  }

  /**
   * @inheritdoc
   */
  function getBook($long = FALSE) {
    return 'Solutions Elementary (units I – 4)';
  }

  /**
   * @return string
   */
  function getXlsx() {
    return '08_New_Opportunities_Beginner_2.xlsx';
  }

  /**
   * @return array
   */
  function getXlsxCoordinates() {
    return [
      'start' => 'A7',
      'class_number' => 'B3',
      'start_date' => 'C3',
      'end_date' => 'E3',
      'book' => 'G3',
      'level' => 'I3',
    ];
  }

  /**
   * @return integer
   */
  public function getResultIndex() {
    return 15;
  }

}
