<?php

namespace app\components;

use Transliterator\Settings;
use Transliterator\Transliterator;
use Yii;
use yii\base\Component;

class Helper extends Component {

  /**
   * @param $text
   * @return string
   */
  public function transliterate($text) {
    return str_replace(["'", 'ʹ'], '', (new Transliterator(Settings::LANG_RU))->setSystem(Settings::SYSTEM_GOST_2000_B)->cyr2Lat($text));
  }

  /**
   * @return bool
   */
  public function isAdmin() {
    try {
      return Yii::$app->user->getIdentity()->getAuthKey() === 'admin';
    }
    catch (\Throwable $e) {
      return FALSE;
    }
  }
}