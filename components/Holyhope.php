<?php

namespace app\components;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\TransferStats;
use Kevinrob\GuzzleCache\CacheMiddleware;
use Kevinrob\GuzzleCache\Storage\FlysystemStorage;
use Kevinrob\GuzzleCache\Strategy\GreedyCacheStrategy;
use League\Flysystem\Adapter\Local;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use function GuzzleHttp\json_decode as json_decode;

class Holyhope extends Component {
  public $domain;
  public $authkey;
  public $cacheTime = 15;

  /** @var GuzzleClient */
  public $client = NULL;

  /**
   * @inheritDoc
   */
  public function init() {
    if (!isset($this->client)) {
      $this->client = new GuzzleClient([
        'base_uri' => 'https://' . $this->domain . '/Api/V1/',
        'verify' => FALSE,
        'timeout' => 20,
        'headers' => [
          'Content-type' => 'application/json',
        ],
      ]);
    }
  }

  /**
   * @param $uri
   * @param array $query
   * @return mixed
   * @throws \Exception
   */
  protected function request($uri, array $query = []) {
    try {
      $stack = HandlerStack::create();

      if ($this->cacheTime) {
        $storage = new FlysystemStorage(new Local(Yii::getAlias('@runtime/cache-holyhope')));
        $stack->push(new CacheMiddleware(new GreedyCacheStrategy($storage, $this->cacheTime)), 'greedy-cache');
      }

      $request = $this->client->get($uri, [
        'handler' => $stack,
        'query' => array_merge([
          'authkey' => $this->authkey,
        ], $query),
        'on_stats' => function (TransferStats $stats) use (&$url) {
          //echo $stats->getEffectiveUri(), '<br>';die;
        },
      ]);

      return json_decode($request->getBody()->getContents(), TRUE);
    }
    catch (\Exception $e) {
      Yii::error($e->getMessage(), 'holyhope');
      throw $e;
    }
  }

  /**
   * @param array $group
   * @return array
   */
  protected function groupToV1(array $group) {
    $pairs = [
      'School' => 'OfficeOrCompanyName',
      'SchoolId' => 'OfficeOrCompanyId',
      'BeginDate' => 'DateFrom',
      'EndDate' => 'DateTo',
    ];

    foreach ($pairs as $old => $new) {
      if (array_key_exists($new, $group)) {
        $group[$old] = $group[$new];
      }
    }

    return $group;
  }

  /**
   * Получаем список групп
   * @param bool $finished
   * @return array
   * @throws \Exception
   */
  public function getGroups($finished = FALSE) {
    $query = ['types' => 'Group'];

    if ($finished) {
      $query = array_merge($query, [
        'statuses' => 'Finished',
        'dateFrom' => date('Y-m-d', strtotime('now -3 month')),
      ]);
    }

    return $this->groupToV1(ArrayHelper::getValue($this->request('/Api/V2/GetEdUnits', $query), 'EdUnits', []));
  }

  /**
   * Получаем группу
   * @param $id
   * @return array
   * @throws \Exception
   */
  public function getGroup($id) {
    return $this->groupToV1(ArrayHelper::getValue($this->request('/Api/V2/GetEdUnits', ['types' => 'Group', 'id' => $id]), 'EdUnits.0', []));
  }

  /**
   * @param $id
   * @return array
   * @throws \Exception
   */
  public function getGroupStudents($id) {
    $students = ArrayHelper::getValue($this->request('GetLearnerStudents', ['types' => 'Groups', 'learnerId' => $id]), 'LearnerStudents', []);

    $students = array_map(function ($student) {
      $student['_transliterated_name'] = Yii::$app->helper->transliterate($student['StudentName']);
      $student['_hours'] = intval($student['StudyUnits']);
      return $student;
    }, $students);

    ArrayHelper::multisort($students, 'StudentName', SORT_ASC, SORT_LOCALE_STRING);

    return $students;
  }

  /**
   * @param $id
   * @return mixed
   * @throws \Exception
   */
  public function getStudent($id) {
    return ArrayHelper::getValue($this->request('GetStudents', ['id' => $id]), 'Students.0', []);
  }
}