<?php

namespace app\components;

use GuzzleHttp\Psr7\Response;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

class GoogleDrive extends Component {
  public $authCode = '';

  /**
   * @inheritDoc
   */
  public function init() {
    parent::init();
  }

  /**
   * @return \Google_Client|\yii\console\Response|\yii\web\Response
   * @throws \Google_Exception
   * @throws \Exception
   */
  public function getClient() {
    $client = new \Google_Client();
    $client->setApplicationName('Google Drive API PHP Quickstart');
    $client->setScopes([
      \Google_Service_Drive::DRIVE_FILE,
      \Google_Service_Drive::DRIVE_METADATA,
      \Google_Service_Drive::DRIVE_APPDATA,
      \Google_Service_Sheets::SPREADSHEETS,
    ]);
    $client->setAuthConfig(Yii::getAlias('@app') . '/storage/credentials.json');
    $client->setAccessType('offline');
    $client->setPrompt('select_account consent');

    // Load previously authorized token from a file, if it exists.
    // The file token.json stores the user's access and refresh tokens, and is
    // created automatically when the authorization flow completes for the first
    // time.
    $tokenPath = Yii::getAlias('@app') . '/storage/token.json';
    if (file_exists($tokenPath)) {
      $accessToken = json_decode(file_get_contents($tokenPath), TRUE);
      $client->setAccessToken($accessToken);
    }
    else {
      //echo $client->createAuthUrl();
      //die;
    }

    // If there is no previous token or it's expired.
    if ($client->isAccessTokenExpired()) {
      // Refresh the token if possible, else fetch a new one.
      if ($client->getRefreshToken()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
      }
      else {
        if (!$this->authCode) {
          echo $client->createAuthUrl();
          die;
        }

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($this->authCode);
        $client->setAccessToken($accessToken);

        // Check to see if there was an error.
        if (array_key_exists('error', $accessToken)) {
          throw new \Exception(join(', ', $accessToken));
        }
      }
      // Save the token to a file.
      if (!file_exists(dirname($tokenPath))) {
        mkdir(dirname($tokenPath), 0700, TRUE);
      }
      file_put_contents($tokenPath, json_encode($client->getAccessToken()));
    }
    return $client;
  }

  /**
   * @param $xlsx
   * @throws \Google_Exception
   */
  public function xlsxToPdf($xlsx, $pdf) {
    $client = $this->getClient();
    $service = new \Google_Service_Drive($client);

    $file = new \Google_Service_Drive_DriveFile([
      'name' => basename($xlsx),
      'mimeType' => 'application/vnd.google-apps.spreadsheet',
    ]);
    $result = $service->files->create($file, [
      'data' => file_get_contents($xlsx),
      'mimeType' => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      'uploadType' => 'multipart',
    ]);

    /** @var Response $export */
    $export = $service->files->export($result->id, 'application/pdf', ['alt' => 'media']);

    file_put_contents($pdf, $export->getBody()->getContents());

    $service->files->delete($result->id);
  }
}