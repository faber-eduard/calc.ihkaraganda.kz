<?php

namespace app\components;

abstract class CalculatorKids extends Calculator {

  /**
   * @return string
   */
  public function getPdf() {
    return 'form_kids.pdf';
  }

  /**
   * @param $result
   * @param null $course
   * @return string
   */
  public function getAward($result, $course = NULL) {
    return 'has attended ' . $result . ' academic hours of the ' . ($course ?: $this->getBook(TRUE)) . ' Course';
  }

}