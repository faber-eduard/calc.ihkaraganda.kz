<?php

namespace app\components;

use Swift_Events_EventListener;
use Swift_Mime_SimpleMessage;

class MailTransport  implements \Swift_Transport {
  private $_extraParams = '-f%s';

  /** @var \Swift_Events_EventDispatcher */
  private $eventDispatcher;

  /**
   * @inheritDoc
   */
  public function __construct() {
    $this->eventDispatcher = \Swift_DependencyContainer::getInstance()->createDependenciesFor('transport.null')[0];
  }

  /**
   * @inheritdoc
   */
  public function isStarted() {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function start() {

  }

  /**
   * @inheritdoc
   */
  public function stop() {

  }

  /**
   * @inheritdoc
   */
  public function ping() {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function send(Swift_Mime_SimpleMessage $message, &$failedRecipients = NULL) {
    $failedRecipients = (array) $failedRecipients;

    if ($evt = $this->eventDispatcher->createSendEvent($this, $message)) {
      $this->eventDispatcher->dispatchEvent($evt, 'beforeSendPerformed');
      if ($evt->bubbleCancelled()) {
        return 0;
      }
    }

    $count = (count((array) $message->getTo()) + count((array) $message->getCc()) + count((array) $message->getBcc()));

    $toHeader = $message->getHeaders()->get('To');
    $subjectHeader = $message->getHeaders()->get('Subject');

    $to = $toHeader ? $toHeader->getFieldBody() : '';
    $subject = $subjectHeader ? $subjectHeader->getFieldBody() : '';

    $reversePath = $this->getReversePath($message);

    // Remove headers that would otherwise be duplicated
    $message->getHeaders()->remove('To');
    $message->getHeaders()->remove('Subject');

    $messageStr = $message->toString();

    if ($toHeader) {
      $message->getHeaders()->set($toHeader);
    }
    $message->getHeaders()->set($subjectHeader);

    // Separate headers from body
    if (FALSE !== $endHeaders = strpos($messageStr, "\r\n\r\n")) {
      $headers = substr($messageStr, 0, $endHeaders) . "\r\n"; //Keep last EOL
      $body = substr($messageStr, $endHeaders + 4);
    }
    else {
      $headers = $messageStr . "\r\n";
      $body = '';
    }

    unset($messageStr);

    if ("\r\n" != PHP_EOL) {
      // Non-windows (not using SMTP)
      $headers = str_replace("\r\n", PHP_EOL, $headers);
      $subject = str_replace("\r\n", PHP_EOL, $subject);
      $body = str_replace("\r\n", PHP_EOL, $body);
      $to = str_replace("\r\n", PHP_EOL, $to);
    }
    else {
      // Windows, using SMTP
      $headers = str_replace("\r\n.", "\r\n..", $headers);
      $subject = str_replace("\r\n.", "\r\n..", $subject);
      $body = str_replace("\r\n.", "\r\n..", $body);
      $to = str_replace("\r\n.", "\r\n..", $to);
    }

    if (mail($to, $subject, $body, $headers, $this->_formatExtraParams($this->_extraParams, $reversePath))) {
      if ($evt) {
        $evt->setResult(\Swift_Events_SendEvent::RESULT_SUCCESS);
        $evt->setFailedRecipients($failedRecipients);
        $this->eventDispatcher->dispatchEvent($evt, 'sendPerformed');
      }
    }
    else {
      $failedRecipients = array_merge($failedRecipients, array_keys((array) $message->getTo()), array_keys((array) $message->getCc()), array_keys((array) $message->getBcc()));

      if ($evt) {
        $evt->setResult(\Swift_Events_SendEvent::RESULT_FAILED);
        $evt->setFailedRecipients($failedRecipients);
        $this->eventDispatcher->dispatchEvent($evt, 'sendPerformed');
      }

      $message->generateId();

      $count = 0;
    }

    return $count;
  }

  /**
   * @inheritdoc
   */
  public function registerPlugin(Swift_Events_EventListener $plugin) {
    return NULL;
  }

  /**
   * Return php mail extra params to use for invoker->mail.
   * @param $extraParams
   * @param $reversePath
   * @return string|null
   */
  private function _formatExtraParams($extraParams, $reversePath) {
    if (FALSE !== strpos($extraParams, '-f%s')) {
      if (empty($reversePath)) {
        $extraParams = str_replace('-f%s', '', $extraParams);
      }
      else {
        $extraParams = sprintf($extraParams, $reversePath);
      }
    }

    return !empty($extraParams) ? $extraParams : NULL;
  }

  /**
   * @param \Swift_Mime_SimpleMessage $message
   * @return int|null|string
   */
  protected function getReversePath(\Swift_Mime_SimpleMessage $message) {
    $return = $message->getReturnPath();
    $sender = $message->getSender();
    $from = $message->getFrom();
    $path = NULL;
    if (!empty($return)) {
      $path = $return;
    }
    elseif (!empty($sender)) {
      if (!is_array($sender)) {
        $sender = (array) $sender;
      }

      reset($sender); // Reset Pointer to first pos
      $path = key($sender); // Get key
    }
    elseif (!empty($from)) {
      reset($from); // Reset Pointer to first pos
      $path = key($from); // Get key
    }

    return $path;
  }
}