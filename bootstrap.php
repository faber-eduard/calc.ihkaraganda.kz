<?php

define('REQUEST_TIME', time());
set_time_limit(120);

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 */
class Yii extends \yii\BaseYii {
  /**
   * @var BaseApplication the application instance
   */
  public static $app;
}

spl_autoload_register(['Yii', 'autoload'], TRUE, TRUE);
Yii::$classMap = include(__DIR__ . '/vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container;

/**
 * Class BaseApplication
 * @property \yii\web\User $user
 * @property \yii\web\Session $session
 * @property \yii\web\View $view
 * @property \app\components\Holyhope $holyhope
 * @property \app\components\GoogleDrive $googleDrive
 * @property \app\components\Helper $helper
 * @property string $homeUrl
 */
abstract class BaseApplication extends yii\base\Application {

}
